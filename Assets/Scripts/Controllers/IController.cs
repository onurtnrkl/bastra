#region License
/*================================================================
Product:    Bastra
Developer:  Onur Tanrıkulu
Date:       19/09/2017 11:17

Copyright (c) 2017 Onur Tanrikulu. All rights reserved.
================================================================*/
#endregion

public interface IController
{
    void Init();
    void Restart();
}
